(function(window, ng) {
    'use strict';

    ng.module('PatientsListTest').factory('PatientsService', PatientsService);

    PatientsService.$inject = [
        '$http'
    ];

    function PatientsService($http) {
        var PatientsService = {};

        PatientsService.one = function (itemId) {
            return $http.get('/patients/' + itemId)
                .then(prepareResponse)
                .then(function (response) {
                    if (response.dob) {
                        response.dob = new Date(response.dob);
                    }
                    return response;
                });
        };
        
        PatientsService.update = function (item) {
            if (ng.isObject(item.dob) && ng.isFunction(item.dob.valueOf)) {
                item.dob = item.dob.valueOf();
            }
            return $http.put('/patients/' + item.id, item);
        };
        
        return PatientsService;
        
        function prepareResponse(response) {
            return response.data;
        }
    }
})(this, this.angular);
