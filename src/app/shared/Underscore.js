(function (window, ng) {
    'use strict';

    ng.module('PatientsListTest').factory('_', ['$window', function ($window) {
        return $window._.noConflict();
    }]);

})(window, window.angular);
