(function(window, ng) {
    'use strict';

    ng.module('PatientsListTest').directive('focusOnEvent', focusOnEvent);
    ng.module('PatientsListTest').directive('isValidDate', isValidDate);
    
    focusOnEvent.$inject = [
        '$timeout'
    ];
    function focusOnEvent($timeout) {
        return {
            restrict: 'A',
            link: function(scope, el, attrs) {
                ng.forEach(attrs.focusOnEvent.split(' '), function(eventName) {
                    scope.$on(eventName, function() {
                        $timeout(function() {
                            el[0].focus();
                        });
                    });
                });
            }
        };
    }
    
    
    function isValidDate() {
        return {
            require: 'ngModel',
            link: function(scope, el, attrs, ctrl) {
                ctrl.$validators.validDate = function (modelValue, viewValue) {
                    return ng.isObject(modelValue);
                };
            }
        };
    }
})(this, this.angular);
