(function(window, ng) {
    'use strict';

    ng.module('PatientsListTest').factory('DiagnosesService', DiagnosesService);

    DiagnosesService.$inject = [
        '$http'
    ];

    function DiagnosesService($http) {
        var DiagnosesService = {};
        
        DiagnosesService.list = function () {
            return $http.get('/diagnoses')
                    .then(prepareResponse)
                    .then(function (response) {
                        return response.map(prepareItem);
                    });
        };

        DiagnosesService.one = function (itemId) {
            return $http.get('/diagnoses/' + itemId)
                    .then(prepareResponse)
                    .then(prepareItem); 
        };
        
        DiagnosesService.update = function (item) {
            if (ng.isDate(item.dt_added)) {
                item.dt_added = item.dt_added.valueOf();
            }
            if (ng.isDate(item.is_deleted)) {
                item.is_deleted = item.is_deleted.valueOf();
            }
            return $http.put('/diagnoses/' + item.id, item);
        };
        
        DiagnosesService.create = function (item) {
            item.dt_added = Date.now();
            return $http.post('/diagnoses', item);
        };
        
        DiagnosesService.delete = function (item) {
            item.is_deleted = true;
            item.dt_deleted = Date.now();
            return DiagnosesService.update(item);
        };
        
        return DiagnosesService;
        
        function prepareResponse(response) {
            return response.data;
        }
        
        function prepareItem(anItem) {
            if (anItem.dt_added) {
                anItem.dt_added = new Date(anItem.dt_added);
            }
            if (anItem.is_deleted) {
                anItem.is_deleted = new Date(anItem.is_deleted);
            }
            return anItem;
        }
    }
})(this, this.angular);
