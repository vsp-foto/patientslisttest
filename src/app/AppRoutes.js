(function(window, ng) {
    'use strict';

    ng.module('PatientsListTest')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
                .state('pageNotFound', {
                    url: '/notfound',
                    templateUrl: 'app/components/notfound/notfound.html',
                    controller: ['AppTitle', function(AppTitle) {
                        AppTitle.set('Page not found');
                    }]
                })
                    
                .state('patient', {
                    url: '/patient/:patientId',
                    abstract: true,
                    templateUrl: 'app/components/patient/patient.html',
                    controller: 'PatientController',
                    controllerAs: 'vmPatient',
                    resolve: {
                        patient: ['PatientsService', '$stateParams', function (PatientsService, $stateParams) {
                            return PatientsService.one($stateParams.patientId);
                        }]
                    }
                })
                    
                .state('patient.diagnosesList', {
                    url: '/diagnoses-list',
                    templateUrl: 'app/components/diagnosesList/diagnosesList.html',
                    controller: 'DiagnosesListController',
                    controllerAs: 'vmList',
                    resolve: {
                        items: ['DiagnosesService', function (DiagnosesService) {
                            return DiagnosesService.list();
                        }]
                    }
                })
                
                .state('patient.editDiagnose', {
                    url: '/edit-diagnose/:itemId',
                    templateUrl: 'app/components/editDiagnose/editDiagnose.html',
                    controller: 'EditDiagnoseController',
                    controllerAs: 'vmEdit',
                    resolve: {
                        isEditMode: function () {
                            return true;
                        },
                        item: ['DiagnosesService', '$stateParams', function (DiagnosesService, $stateParams) {
                            return DiagnosesService.one($stateParams.itemId);
                        }]
                    }
                })
                
                .state('patient.addDiagnose', {
                    url: '/add-diagnose',
                    templateUrl: 'app/components/editDiagnose/editDiagnose.html',
                    controller: 'EditDiagnoseController',
                    controllerAs: 'vmEdit',
                    resolve: {
                        isEditMode: function () {
                            return false;
                        },
                        item: function () {
                            return {};
                        }
                    }
                })
                
                .state('patient.edit', {
                    url: '/edit/:itemId',
                    templateUrl: 'app/components/editPatient/editPatient.html',
                    controller: 'EditPatientController',
                    controllerAs: 'vmEditPatient',
                    resolve: {
                        item: ['PatientsService', '$stateParams', function (PatientsService, $stateParams) {
                            return PatientsService.one($stateParams.itemId);
                        }]
                    }
                });
    }]);
})(this, this.angular);
