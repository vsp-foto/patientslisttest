(function(window, ng) {
    'use strict';

    ng.module('PatientsListTest', [
        'BackendMock',
        'datePicker',
        'LocalStorageModule',
        'templates',
        'ui.router'
    ])
    .config(Config).run(Run);
    
    
    Config.$inject = [
        '$locationProvider',
        '$urlRouterProvider'
    ];
    function Config($locationProvider, $urlRouterProvider) {
        $locationProvider.html5Mode(true).hashPrefix('!');
        
        $urlRouterProvider
                .when('/',           '/patient/1/diagnoses-list')
                .when('/patient/1',  '/patient/1/diagnoses-list')
                .otherwise(function($injector, $location) {
                    // Prevent the UI Router from going into infinite loop
                    $injector.get('$state').go('pageNotFound');
                });
    }
    
    
    Run.$inject = [
        '$rootScope'
    ];
    function Run($rootScope) {
        $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
            console.log(error);
        });
    }
})(this, this.angular);
