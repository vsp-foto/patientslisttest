(function(window, ng) {
    'use strict';

    ng.module('PatientsListTest').controller('DiagnosesListController', DiagnosesListController);

    DiagnosesListController.$inject = [
        '$scope',
        '$state',
        'DiagnosesService',
        'AppTitle',
        'items'
    ];

    function DiagnosesListController($scope, $state, DiagnosesService, AppTitle, items) {
        var vmList = this;
        
        AppTitle.set('Diagnoses');
        
        vmList.deleteItem = deleteItem;
        
        init();
        
        function init() {
            vmList.currentItems = [];
            vmList.historyItems = [];
            items.forEach(function (anItem) {
                if (anItem.is_deleted) {
                    vmList.historyItems.push(anItem);
                } else {
                    vmList.currentItems.push(anItem);
                }
            });
        }
        
        function deleteItem(anItemToDelete) {
            DiagnosesService.delete(anItemToDelete)
                    .then(function () {
                        // Move item to the history items
                        vmList.historyItems.push(anItemToDelete);
                        vmList.currentItems = vmList.currentItems.filter(function (anItem) {
                            return anItem.id !== anItemToDelete.id;
                        });
                    });
        }
    }
})(this, this.angular);
