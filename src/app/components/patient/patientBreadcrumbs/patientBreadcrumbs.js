(function(window, ng) {
    'use strict';

    ng.module('PatientsListTest').component('patientBreadcrumbs', {
        templateUrl: 'app/components/patient/patientBreadcrumbs/patient-breadcrumbs.html',
        bindings: {
            patient: '<'
        }
    });
})(this, this.angular);
