(function(window, ng) {
    'use strict';

    ng.module('PatientsListTest').controller('PatientController', PatientController);

    PatientController.$inject = [
        '$scope',
        'patient'
    ];

    function PatientController($scope, patient) {
        var vmPatient = this;
        
        vmPatient.patient = patient;
        
        $scope.$on('updatePatientInfo', updatePatientInfo);
        
        function updatePatientInfo(e, data) {
            vmPatient.patient = data.patient;
        }
    }
})(this, this.angular);
