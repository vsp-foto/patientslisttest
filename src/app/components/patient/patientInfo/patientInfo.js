(function(window, ng) {
    'use strict';

    ng.module('PatientsListTest').component('patientInfo', {
        templateUrl: 'app/components/patient/patientInfo/patient-info.html',
        bindings: {
            patient: '<'
        }
    });
})(this, this.angular);
