(function(window, ng) {
    'use strict';

    ng.module('PatientsListTest').controller('EditPatientController', EditPatientController);

    EditPatientController.$inject = [
        '$rootScope',
        '$scope',
        '$state',
        'item',
        'PatientsService',
        'AppTitle'
    ];

    function EditPatientController($rootScope, $scope, $state, item, PatientsService, AppTitle) {
        var vmEditPatient = this;
        
        AppTitle.set('Edit patient');
        
        vmEditPatient.item = item;
        
        vmEditPatient.submit = submit;
        
        $scope.$applyAsync(function() {
            $scope.$broadcast('focus.name');
        });
        
        
        function submit() {
            if ($scope.EditPatientForm.$invalid) return;
            PatientsService.update(vmEditPatient.item)
                    .then(function () {
                        $state.go('patient.diagnosesList');
                        $rootScope.$broadcast('updatePatientInfo', { patient: vmEditPatient.item });
                    });
        }
    }
})(this, this.angular);
