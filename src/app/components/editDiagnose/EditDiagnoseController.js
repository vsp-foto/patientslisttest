(function(window, ng) {
    'use strict';

    ng.module('PatientsListTest').controller('EditDiagnoseController', EditDiagnoseController);

    EditDiagnoseController.$inject = [
        '$scope',
        '$state',
        'item',
        'isEditMode',
        'DiagnosesService',
        'AppTitle'
    ];

    function EditDiagnoseController($scope, $state, item, isEditMode, DiagnosesService, AppTitle) {
        var vmEdit = this;
        
        AppTitle.set('Edit diagnose');
        
        vmEdit.isEditMode = isEditMode;
        vmEdit.item = item;
        
        vmEdit.submit = submit;
        
        $scope.$applyAsync(function() {
            $scope.$broadcast('focus.code');
        });
        
        function submit() {
            var promise;
            if ($scope.EditDiagnoseForm.$invalid) return;
            
            if (vmEdit.isEditMode) {
                promise = DiagnosesService.update(vmEdit.item);
            } else {
                promise = DiagnosesService.create(vmEdit.item);
            }
            promise.then(function () {
                $state.go('patient.diagnosesList');
            });
        }
    }
})(this, this.angular);
