(function(window, ng) {
    'use strict';

    ng.module('BackendMock').run(configureDiagnoses);
    
    
    configureDiagnoses.$inject = [
        '$httpBackend',
        'localStorageService',
        '_'
    ];
    function configureDiagnoses($httpBackend, localStorageService, _) {
        var STORAGE_KEY = 'diagnoses';
        var items = getItemsFromLocalStorage();
        
        // Get single item
        $httpBackend.whenGET(/\/diagnoses\/\w+/).respond(function (method, url, params) {
            var itemId = getItemIdFromUrl(url);
            return [200, getItemById(itemId), {}];
        });
        
        // Get all items
        $httpBackend.whenGET('/diagnoses').respond(function(method, url, data) {
            return [200, items, {}];
        });
        
        // Create item
        $httpBackend.whenPOST('/diagnoses').respond(function(method, url, data) {
            var item = ng.fromJson(data);
            item.id = createUniqueId();
            items.push(item);
            saveItemsToLocalStorage(items);
            return [200, item, {}];
        });
        
        // Update item
        $httpBackend.whenPUT(/\/diagnoses\/\w+/).respond(function (method, url, data) {
            var itemId = getItemIdFromUrl(url);
            var modifiedItem = ng.fromJson(data);
            items.some(function (anItem, i) {
                if (anItem.id !== itemId) return;
                items[i] = modifiedItem;
                return true;
            });
            saveItemsToLocalStorage(items);
            return [200, modifiedItem, {}];
        });
        
        
        function getItemsFromLocalStorage() {
            var result = localStorageService.get(STORAGE_KEY);
            if (!result) {
                // First time run
                result = [];
                localStorageService.set(STORAGE_KEY, result);
            }
            return result;
        }
        
        function saveItemsToLocalStorage(items) {
            localStorageService.set(STORAGE_KEY, items);
        }
        
        function getItemById(itemId) {
            return _.findWhere(items, { id: itemId });
        }
        
        function getItemIdFromUrl(url) {
            return url.match(/\/diagnoses\/(\w+)$/)[1];
        }
    }
    
    function createUniqueId() {
        return Date.now().toString() + parseInt(performance.now() * 1000, 10);
    }
})(this, this.angular);
