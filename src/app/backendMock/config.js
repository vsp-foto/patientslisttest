(function(window, ng) {
    'use strict';

    ng.module('BackendMock', [
        'ngMockE2E',
        'LocalStorageModule'
    ])
    .config(Config)
    .run(Run);
    
    
    Config.$inject = [
        'localStorageServiceProvider'
    ];
    function Config(localStorageServiceProvider) {
        localStorageServiceProvider.setPrefix('PatientsListTest');
    }
    
    Run.$inject = [
        '$httpBackend'
    ];
    function Run($httpBackend) {
        // Don't mock requests to HTML files
        $httpBackend.whenGET(/.+\.html$/).passThrough();
    }
})(this, this.angular);
