# Glorium test task

### Install:
> `npm i`

> `bower install`

### Run dev version locally with livereload
> `gulp serve`

Go to http://localhost:3000 url.


### Build
> `gulp build`

Production-ready files will be saved into `dist` folder.


### Run built version locally
> `gulp serve_dist`

Go to http://localhost:3001