var gulp           = require('gulp');
var webserver      = require('gulp-webserver');
var less           = require('gulp-less');
var rename         = require('gulp-rename');
var watch          = require('gulp-watch');
var batch          = require('gulp-batch');
var spa            = require('gulp-spa');
var uglify         = require('gulp-uglify');
var htmlmin        = require('gulp-htmlmin');
var concat         = require('gulp-concat');
var rev            = require('gulp-rev');
var tap            = require('gulp-tap');
var path           = require('path');
var cleanCSS       = require('gulp-clean-css');
var templateCache  = require('gulp-angular-templatecache');
var del            = require('del');
var flatten        = require('gulp-flatten');
var cssUrlAdjuster = require('gulp-css-url-adjuster');

var BASE_PATH_SRC = ['src', '!src/less/**/*'];
var BUILD_BASE_PATH_SRC = 'src';
var BASE_PATH_DIST = 'dist';

var libsFNameWithRev = {
    libs: '', css: '', tpl: ''
};

gulp.task('serve', ['watch-less', 'compile-less'], function() {
    gulp.src(BASE_PATH_SRC)
            .pipe(webserver({
                livereload: {enable: true, port: 35728},
                open: 'http://localhost:3000/',
                port: 3000,
                fallback: 'index.html'
            }));
});

gulp.task('watch-less', function () {
    watch('src/less/**/*', batch(function (events, done) {
        gulp.start('compile-less', done);
    }));
});

gulp.task('compile-less', function () {
  return gulp.src('src/less/main.less')
    .pipe(less())
    .pipe(rename('app.css'))
    .pipe(gulp.dest('src/assets'));
});



// --------------------------------------------

gulp.task('serve_dist', function() {
    gulp.src(BASE_PATH_DIST)
            .pipe(webserver({
                livereload: false,
                open: true,
                port: 3001,
                fallback: 'index.html'
            }));
});

gulp.task('build-clear', function(callback) {
    return del([BASE_PATH_DIST + '/**', '!' + BASE_PATH_DIST], {force: true}, callback);
});

gulp.task('build-app', ['build-clear'], function() {
    return gulp.src(BUILD_BASE_PATH_SRC + '/index.html')
        .pipe(spa.html({
            assetsDir: BUILD_BASE_PATH_SRC,
            pipelines: {
                main: function(files) {
                    // this gets applied for the HTML file itself 
                    return files.pipe(htmlmin());
                },
                libs_js: function(files) {
                    return files
                        .pipe(concat('libs.js'))
                        .pipe(rev())
                        .pipe(tap(saveFNameWithRev('libs', '.js')));
                },
                app_js: function(files) {
                    return files
                        .pipe(uglify())
                        .pipe(concat('app.js'))
                        .pipe(rev())
                        .pipe(tap(saveFNameWithRev('css', '.css')));
                },
                css: function(files) {
                    return files
                        .pipe(concat('app.css'))
                        .pipe(cssUrlAdjuster({
                            replace: ['../', ''],
                            prepend: '/assets/'
                        }))
                        .pipe(cleanCSS())
                        .pipe(rev())
                        .pipe(tap(saveFNameWithRev('css', '.css')));
                },
                ng_templates: function() {
                    return gulp.src(BUILD_BASE_PATH_SRC + '/app/**/*.html')
                        .pipe(htmlmin({collapseWhitespace: true, conservativeCollapse: true, collapseInlineTagWhitespace: true}))
                        .pipe(templateCache({root: 'app/'}))
                        .pipe(uglify())
                        .pipe(rev())
                        .pipe(tap(saveFNameWithRev('tpl', '.js')));
                }
            }
        }))
        .pipe(gulp.dest(BASE_PATH_DIST));

    function saveFNameWithRev(key, ext) {
        return function(file) {
            if (path.extname(file.path) === ext) {
                libsFNameWithRev[key] = path.basename(file.path);
            }
        };
    }
});

gulp.task('build-copy-images', ['build-clear'], function(callback) {
    return gulp.src([BUILD_BASE_PATH_SRC + '/assets/img/**/*'])
            .pipe(gulp.dest(BASE_PATH_DIST + '/assets/img/'));
});

gulp.task('build-copy-fonts', ['build-clear'], function() {
    return gulp.src(BUILD_BASE_PATH_SRC + '/assets/**/*.{otf,eot,svg,ttf,woff,woff2}')
            .pipe(flatten())
            .pipe(gulp.dest(BASE_PATH_DIST + '/assets/fonts/'));
});

gulp.task('build-copy-fonts', ['build-clear'], function() {
    return gulp.src(BUILD_BASE_PATH_SRC + '/assets/**/*.{otf,eot,svg,ttf,woff,woff2}')
            .pipe(flatten())
            .pipe(gulp.dest(BASE_PATH_DIST + '/assets/fonts/'));
});

gulp.task('build', [
        'build-app',
        'build-copy-images',
        'build-copy-fonts'
    ], function(callback) {
        callback(null);
    });